from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response

def index(request):
	context = RequestContext(request)
	context_dict = {'title':'Civic Reports'}
	return render_to_response('reports/index.html',context_dict,context)